package world;


import java.awt.Color;

public class Player0 extends Sprite
{

    int counter = 0;
    public Player0(int x, int y, double bearing, Arena arena, int id) 
    {
        super(x, y, 50, bearing, Color.MAGENTA, false, arena, id);
    }
    
    public void move() 
    {
    	go(0, 1, 1);
    	counter++;
    	if(counter > 10 )
    	{
    		counter = 0;
    		fire();
    	}
    }
}
