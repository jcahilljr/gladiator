package world;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class Arena extends JPanel implements Runnable
{
	private final int DELAY = 25;

    private ArrayList<Sprite> players;
	private Thread animator;

    private boolean ingame;
   
    private final int B_WIDTH = 1500;
    private final int B_HEIGHT = 1000;

    private boolean update = true;
    private int bulletDamage = 10;
    
    Random rand = new Random();

    
    InputEvent input;
    
    int initPlayerCount = 100;
    int playerCount;
    
    public Arena() {
    	addKeyListener(new TAdapter(this));
    	setFocusable(true);
        setBackground(Color.GRAY);
        setDoubleBuffered(true);
        ingame = true;
        
        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
        players = new ArrayList<>();
        
        initializePlayers(initPlayerCount);
        
        playerCount = players.size();
        System.out.println(playerCount + " begin");
        
    }
    
    public void initializePlayers(int initplayerCount)
    {
    	update = true;
    	
    	players.clear();
    	
    	
    	for (int i = 0; i < initplayerCount; i++)
    	{
	    	players.add(new Player0(rand.nextInt(B_WIDTH - 50), rand.nextInt(B_HEIGHT-50), rand.nextInt(360), this, players.size()));
	    	players.add(new Player1(rand.nextInt(B_WIDTH - 50), rand.nextInt(B_HEIGHT-50), rand.nextInt(360), this, players.size()));
	    	players.add(new Player2(rand.nextInt(B_WIDTH - 50), rand.nextInt(B_HEIGHT-50), rand.nextInt(360), this, players.size()));
	    	
    	}
    	playerCount = players.size();
    	System.out.println(playerCount + " PLAYERS INITIALIZE! ");
    }

    @Override
    public void addNotify() {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        if (ingame) 
        {
            drawObjects(g, g2d);
        }
        else 
        {
            drawGameOver(g);
        }
    }

    private void drawObjects(Graphics g, Graphics2D g2d) {
    	drawData(g);
    	
    	if(playerCount <= 1)
    	{
    		System.out.println(playerCount + " end");
    		update = false;
    		drawGameOver(g);
    	}
    	
    	for (int i = 0; i < players.size(); i++)
    	{
    		Sprite p = players.get(i);
    		p.drawPlayer(g,g2d);
    		
    		ArrayList<Missile> ms = p.getMissiles();
    		for (Missile m : ms) 
    		{
                m.drawPlayer(g, g2d);
            }
    	}
    	
    }

    private void drawGameOver(Graphics g) 
    {
        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics fm = getFontMetrics(small);

        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (B_WIDTH - fm.stringWidth(msg)) / 2, B_HEIGHT / 2);
    }
    
    private void drawData(Graphics g)
    {
    	g.setColor(Color.white);
    	g.setFont(new Font("Helvetica", Font.BOLD, 14));
    	    	
    	
    	int xpos = 10;
    	int ypos = 0;
    	for (int i = 0; i < players.size(); i++)
    	{
    		
    		ypos += 15;
    		if(ypos > B_HEIGHT)
    		{
    			ypos = 15;
    			xpos += 250;
    		}
    		if (players.get(i).isDead())
    		{
    			g.setColor(Color.BLACK);
    		}
    		else if (players.get(i).getHealth() < 25)
    		{
    			g.setColor(Color.RED);
    		}
    		else
    		{
    			g.setColor(Color.WHITE);
    		}
    		String player = "Player " + i + ": " + players.get(i).getHealth() + "/100 Shots Fired: " + players.get(i).getShotsFired();
    		g.drawString(player, xpos, ypos);
    	}

    }
       
	public void updatePlayers()
	{
		
    	for (int i = 0; i < players.size(); i++)
    	{
    		Sprite p = players.get(i);
    		if(p.isVisible())
    		{
        		p.move();
    		}
    		else
    		{

    	
    		}
    	}
    }
	
	public void updateMissiles()
	{
		for (int i = 0; i < players.size(); i++)
    	{
			Sprite p = players.get(i);
	        ArrayList<Missile> ms = p.getMissiles();

	        for (int j = 0; j < ms.size(); j++) {

	            Missile m = ms.get(j);

	            if (m.isVisible()) {
	                m.move();
	            } else {
	                ms.remove(j);
	            }
	        }
    	}
	}

	
	public void checkCollisions() 
	{
    	for (int i = 0; i < players.size(); i++)
    	{
    		Sprite p = players.get(i);
    		p.checkCollision(this);
    		for (int j = 0; j < players.size(); j++)
    		{
    			if(i != j && !p.isDead())
    			{
    				if (!players.get(j).isDead())
    				{
    					p.checkCollision(players.get(j), j, false);
    				}
    		
	    			ArrayList<Missile> ms = players.get(j).getMissiles();
	
	    	        for (int k = 0; k < ms.size(); k++)
	    	        {
	
	    	            Missile m = ms.get(k);	            
	    	            if (p.checkCollision(m, i, true))
	    	            {
	    	            	m.setVisible(false);
	    	            	p.damage(bulletDamage);
	    	            }
	    	        }
    			}
    		}
    		
    	}
    }
	
	public int getWidth()
	{
		return B_WIDTH;
	}
	
	public int getHeight()
	{
		return B_HEIGHT;
	}
	
	public void decreasePlayerCount()
	{
		playerCount--;
	}
	
	public int getPlayerCount()
	{
		return initPlayerCount;
	}
	

    @Override
    public void run() 
    {
        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();

        while (true) 
        {
        
        	if (update)
        	{
            updatePlayers();
            updateMissiles();
            checkCollisions();
        	}
        	
            repaint();
            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;
            if (sleep < 0) 
            {
                sleep = 2;
            }
            try 
            {
                Thread.sleep(sleep);
            } 
            catch (InterruptedException e) 
            {
                System.out.println("Interrupted: " + e.getMessage());
            }
            beforeTime = System.currentTimeMillis();
        }
    }
	
	
    private class TAdapter implements KeyListener {
    	Arena arena;
    	public TAdapter(Arena arena)
    	{
    		this.arena = arena;
    	}
        @Override
        public void keyReleased(KeyEvent e)
        {
            //arena.keyReleased(e);
        	arena.initializePlayers(arena.getPlayerCount());
        }

		@Override
		public void keyPressed(KeyEvent e) {
		
		}

		@Override
		public void keyTyped(KeyEvent e) {
			
			
		}
    }

}