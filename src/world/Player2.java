package world;


import java.awt.Color;
import java.util.Random;

public class Player2 extends Sprite
{
	boolean forward = true;
	boolean rotate = false;
	int counter = 0;
	
	double iBearing = 0;
	double dBearing = 0;
	double randBearing = 0;
	Random randAngle = new Random();
	
     
    public Player2(int x, int y, double bearing, Arena arena, int id) 
    {
        super(x, y, 50, bearing, Color.BLUE, false, arena, id);
    }
    
    public void move() 
    {
    	counter++;
    	if(counter > 10)
    	{
    		counter = 0;
    		fire();
    	}
    	if (forward)
    	{
    		go(3, 0, 0);
    		
        	if (collision)
        	{
        		go(-1,0,0);
        		collision = false;
        		forward = false;
        		rotate = true;
        		iBearing = bearing;
        		randBearing = (randAngle.nextDouble() * 360);
        		
        	}
    	}
    	if (rotate)
    	{
    		go(0, 3, 0);
    		dBearing = Math.abs(iBearing - bearing);
    		if(dBearing >= randBearing)
    		{
    			forward = true;
        		rotate = false;
    		}
    	}
    	
    }
    
}
