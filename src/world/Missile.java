package world;

import java.awt.Color;

public class Missile extends Sprite {

    public Missile(int x, int y, double bearing, Arena arena) {
        super(x, y, 10, bearing, Color.RED, true, arena, -1);

    }
    

    public void move() 
    {
    	go(10, 0, 0);
    }
}
