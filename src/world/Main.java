package world;


import java.awt.EventQueue;
import javax.swing.JFrame;


public class Main extends JFrame {
    
    public Main() {

        initUI();
    }

    private void initUI() {

        add(new Arena());

        setResizable(false);
        pack();
        
        setTitle("Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }    
    
    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Main ex = new Main();
                ex.setVisible(true);
            }
        });
    }
}