package world;


import java.awt.Color;
import java.util.Random;

public class Player1 extends Sprite
{
	boolean forward = true;
	boolean rotate = false;
	int counter = 0;
	
	double iBearing = 0;
	double dBearing = 0;
	Random randAngle = new Random();
	double randBearing = 0;
	
	public Player1(int x, int y, double bearing, Arena arena, int id) 
    {
		super(x, y, 50, bearing, Color.GREEN, false, arena, id);   
    }
    
    public void move() 
    {
    	if (forward)
    	{
    		go(3, 0, 0);
    		counter++;
        	if (collision)
        	{
        		go(-5,0,0);
        		collision = false;
        		forward = false;
        		rotate = true;
        		iBearing = bearing;
        		randBearing = (randAngle.nextDouble() * 360);
        		
        	}
        	if(counter > 10)
        	{
        		counter = 0;
        		fire();
           	}
    	}
    	if (rotate)
    	{
    		go(0, 3, 0);
    		dBearing = Math.abs(iBearing - bearing);
    		if(dBearing >= randBearing)
    		{
    			forward = true;
        		rotate = false;
    		}
    	}
    	
    }
    
}
