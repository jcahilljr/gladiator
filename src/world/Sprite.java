package world;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public abstract class Sprite{

    protected double x;
    protected double y;
    
    protected double dx;
    protected double dy;
    
    protected int diameter;
    
    protected int speed;
    
    protected double bearing;    
    protected boolean vis;
    
    protected boolean right= false;
    protected boolean left = false;
    protected boolean top = false;
    protected boolean bottom = false;
    
    boolean collision = false;
    
    boolean missile = false;
    
    int health = 100;
    boolean dead = false;
    
    int shotsFired = 0;
    
    protected Color color;
 
    private ArrayList<Missile> missiles;
    
    private Arena arena; 
    
    private int id;

    public Sprite(int x, int y, int diameter, double bearing, Color color, boolean missile, Arena arena, int id)
    {
        this.x = x;
        this.y = y;
        this.bearing = bearing;
        this.diameter = diameter;
        vis = true;
        this.color = color;
        this.missile = missile;
        missiles = new ArrayList<>();
        this.arena = arena;
        this.id = id;
    }

    public abstract void move();
    
    public void fire() {
        missiles.add(new Missile((int)getCenterX(), (int)getCenterY(), this.bearing, arena));
        //System.out.println("FIRE");
        shotsFired++;
    }
    
    public ArrayList getMissiles() {
        return missiles;
    }
    
    
    public void go(int throttle, int turn, int strafe)
    {
    	bearing += turn;

		dx = throttle * Math.cos(Math.toRadians(bearing)) + strafe * Math.sin(Math.toRadians(bearing));
		dy = throttle * Math.sin(Math.toRadians(bearing)) - strafe * Math.cos(Math.toRadians(bearing));

		if (right && dx > 0)
		{
			dx = 0;
			right = false;
		}
		if (left && dx < 0)
		{
			dx = 0;
			left = false;
		}
		if (top && dy < 0)
		{
			top = false;
			dy = 0;
		}
		if (bottom && dy > 0)
		{
			bottom = false;
			dy = 0;
		}
		
        x += dx;
        y += dy;        

        //System.out.printf("%f, %f, %f, %f, %d, %f\n", dx, dy, x, y, turn, bearing);

    }
    
    public void checkCollision(Arena world)
    {
    	if (x + diameter >= world.getWidth())
        {
    		right = true;
        	pressureSensor("right");
        }
        if (x <= 0)
        {
        	left = true;
        	pressureSensor("left");
        }
        if (y + getD() >= world.getHeight())
        {
        	bottom = true;
        	pressureSensor("bottom");
        }
        if (y <= 0)
        {
        	top = true;
        	pressureSensor("top");
        }
    }
    
    public boolean checkCollision(Sprite player, int i, boolean missile )
    {

    	int x1 = this.getCenterX();
    	int y1 = this.getCenterY();
    	
    	int x2 = player.getCenterX();
    	int y2 = player.getCenterY();
    	
    	double distance = Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    	//System.out.println(distance + ", " + i);
    	double space = this.getD()/2+player.getD()/2;
    	
    	if (distance <= space)
    	{
    		if(missile)
    		{
    			//System.out.printf("PLAYER %d HIT!\n", i);
    			return true;
    		}
    		else
    		{
    			//System.out.println("Player Intersection " + distance + ", " + space);
    			return true;
    		}
    	}
    	return false;
    }
    
    public void pressureSensor(String direction)
    {
    	collision = true;
    	//System.out.println("felt something at " + x +", " + y + " towards the " + direction + " of the world");
    }
    
    public void drawPlayer(Graphics g, Graphics2D g2d) {
        if (isVisible()) 
        {
        	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        	
        	AffineTransform old = g2d.getTransform();

            g2d.setColor(color);

            // rotating the hero, rotation point is the middle of the square
            g2d.rotate(getBearing(),getCenterX(),getCenterY());
            
            // drawing the circle
            g2d.fillOval((int)x, (int)y, diameter, diameter);

            // to know the "head"
            if(!missile)
            {
            	g2d.setColor(Color.WHITE);
            	g2d.fillOval((int)x+diameter-10, (int)y+10, 10, 30);
            	
            	g.setColor(Color.white);
            	g.setFont(new Font("Helvetica", Font.BOLD, 14));
            	g.drawString(Integer.toString(id), (int)getCenterX()-10, (int)getCenterY()+5 );
           	}
            //in case you have other things to rotate
            g2d.setTransform(old);
            
        }
    }
    
    public int getHealth()
    {
    	return health;
    }
    
    public void damage(int damage)
    {
    	health -= damage;
    	if (health <= 0)
    	{
    		this.setVisible(false);
    		this.setDead();
    	}
    }
   
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public boolean isVisible() {
        return vis;
    }

    public void setVisible(Boolean visible) {
        vis = visible;
    }
    
    public int getCenterX()
    {
    	return (int)(x+(diameter/2));
    }
    
    public int getCenterY()
    {
    	return (int)(y+(diameter/2));
    }
    
    public double getBearing()
    {
    	return Math.toRadians(bearing);
    }
    
    public int getD()
    {
    	return diameter;
    }
    
    
    public Rectangle getBounds()
    {
    	return new Rectangle((int)x,(int)y, diameter,diameter);
    }
    
    public void setDead()
    {
    	dead = true;
    	arena.decreasePlayerCount();
    }
    
    public boolean isDead()
    {
    	return dead;
    }
    
    public int getShotsFired()
    {
    	return shotsFired;
    }
}